import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  // { path: 'signup', loadChildren: './signup-permission/signup-permission.module#SignupPermissionPageModule' },
  { path: 'owner', loadChildren: './owner/owner.module#OwnerPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'address-details', loadChildren: './signup/address-details/address-details.module#AddressDetailsPageModule' },
  { path: 'bank-account', loadChildren: './signup/bank-account/bank-account.module#BankAccountPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
