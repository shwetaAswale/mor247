import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
selector: 'app-bank-account',
templateUrl: './bank-account.page.html',
styleUrls: ['./bank-account.page.scss'],
})
export class BankAccountPage implements OnInit {
bankAccountForm: FormGroup;

constructor(private formBuilder: FormBuilder, private router: Router) { }

ngOnInit() {
this.bankAccountForm = this.formBuilder.group({
accName: new FormControl(),
accNo: new FormControl(),
ifsc: new FormControl(),
branch: new FormControl(),
upi: new FormControl(),
});
}

}