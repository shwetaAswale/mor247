import { Component, OnInit} from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
// import { UserService } from '../services/user.service';

@Component({
selector: 'app-signup',
templateUrl: './signup.page.html',
styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
createAccontForm: FormGroup;

constructor(private formBuilder: FormBuilder, private router: Router,
public navCtrl: NavController,
// private userService: UserService
) { }

ngOnInit() {
this.createAccontForm = this.formBuilder.group({
entity: new FormControl(),
fullName: new FormControl(),
password: new FormControl(),
mobile: new FormControl(),
emailId: new FormControl(),
gst: new FormControl(),
pan: new FormControl(),
panImageId: new FormControl(),
roleId: 0,
imageId: 0
});
}


}