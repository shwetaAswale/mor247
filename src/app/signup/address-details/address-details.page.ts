import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-address-details',
  templateUrl: './address-details.page.html',
  styleUrls: ['./address-details.page.scss'],
})
export class AddressDetailsPage implements OnInit {

  addressDetailForm: FormGroup;
  obj: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute) {
   }

  ngOnInit() {
    // this.obj = this.activatedRoute.snapshot.data;
    // console.log(this.obj);
    console.log(this.activatedRoute.snapshot.queryParams);
    // this.activatedRoute.queryParams.subscribe(params => {
    // const entity = params['entity'];
    //   console.log(entity);
    // });
    this.addressDetailForm = this.formBuilder.group({
      address1: new FormControl(),
      address2: new FormControl(),
      city: new FormControl(),
      pin: new FormControl(),
      state: new FormControl(),
    });
  }

  next(obj) {
    obj = this.addressDetailForm.value;
    console.log(obj);
  this.router.navigate(['/bank-account'], {queryParams: {obj}, skipLocationChange: true});
  }

}